
class MagicSquaresCounter
  # Class that find and count magic squares.
  def check_if_magic_square?(square)
    # Sums list of columns.
    sumy = square.transpose.map{|e| e.inject(:+)}
    # Sums list of rows.
    sumx = square.map{|e| e.inject(:+)}
    # Diagonal elements sum.
    diagonal = square.map.with_index {|row, i| row[i]} .inject :+
    # Reverted diagonal sum.
    diagonal_rev = square.map.with_index {|row, i| row[-i-1]} .inject :+
    # Array to have all diagonals,rows and columns in one array.
    lines = sumy + sumx
    lines.push diagonal, diagonal_rev
    # Check if every line sum is equal to other.
    lines.uniq.size <= 1
  end

  def magic_squares_count(rectangle)
    # Method find every square and count how many of them are magic.
    row = rectangle.length
    column = rectangle[0].length
    # Counter to know what size of square to search for.
    counter = 0
    magic_squares = 0
    # Square side cannot exceed any rectangle side.
    while counter != row and counter != column
      counter += 1
      for x in 0..row-2
        for y in 0..column-2
          # Will break if it is going to search for square out of rectangle.
          break if (x+counter > row-1 or y+counter > column-1)
          square = []
          # Generate square for passing it to 'check_if_magic_square' method.
          for level in 0..counter
            square.push []
            for line in 0..counter
              square[level].push(rectangle[x+level][y+line])
            end
          end
          if check_if_magic_square? square
            magic_squares += 1
          end
        end
      end
    end
    magic_squares
  end
end

require 'test/unit'

class MyTest < Test::Unit::TestCase
  # Class for testing MagicSquaresCounter methods.

  def setup
    # Initialize
    @squares_count = MagicSquaresCounter.new
  end

  def test_count
    # Check if magic squares are calculated correctly.
    rectangle_5_square = [[4,9,2,3,5],[3,5,7,4,2],[8,1,6,6,2],[1,1,6,6,2],[9,9,9,9,9]]
    rectangle_4_square = [[4,9,2,3,5],[3,5,7,4,2],[8,1,6,6,2],[1,1,6,6,2],[1,1,6,6,2]]
    rectangle_3_square = [[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1]]
    rectangle_2_square = [[4,9,2,3,5],[3,5,7,4,2],[8,1,6,6,2],[1,1,6,6,2]]
    rectangle_1_square = [[4,9,2,3,5],[3,7,7,4,2],[8,1,6,6,2],[1,1,6,6,2]]
    rectangle_0_square = [[4,9,2,3,5],[3,7,7,4,2],[8,1,6,5,2],[1,1,6,6,2]]
    assert_equal(0,  @squares_count.magic_squares_count(rectangle_0_square))
    assert_equal(1,  @squares_count.magic_squares_count(rectangle_1_square))
    assert_equal(2,  @squares_count.magic_squares_count(rectangle_2_square))
    assert_equal(20, @squares_count.magic_squares_count(rectangle_3_square))
    assert_equal(4,  @squares_count.magic_squares_count(rectangle_4_square))
    assert_equal(2,  @squares_count.magic_squares_count(rectangle_5_square))
  end

  def test_check_if_magic_square
    # Check if  magic square is recognized.
    assert_equal(TRUE, @squares_count.check_if_magic_square?([[6,6],[6,6]]))
    assert_equal(FALSE, @squares_count.check_if_magic_square?([[6,1],[6,1]]))
    assert_equal(FALSE, @squares_count.check_if_magic_square?(
        [[6,6,6,6,6,6,6,6,6,6],
         [6,6,6,6,6,6,6,6,6,6],
         [6,6,6,6,6,6,6,6,6,6],
         [6,6,6,6,6,6,6,6,6,6],
         [6,6,6,6,6,6,6,6,6,6],
         [6,6,6,6,6,6,6,6,6,6],
         [6,6,6,6,6,6,4,6,6,6],
         [6,6,6,6,6,6,6,6,6,6],
         [6,6,6,6,6,6,6,6,6,6],
         [6,6,6,6,6,6,6,6,6,6]]
          )
    )
    assert_equal(TRUE, @squares_count.check_if_magic_square?([[4,9,2],[3,5,7],[8,1,6]]))
  end
end